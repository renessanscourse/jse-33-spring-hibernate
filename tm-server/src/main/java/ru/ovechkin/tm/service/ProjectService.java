package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.unknown.ProjectUnknownException;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Transactional
    public void add(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (projectDTO == null) return;
        @NotNull final UserDTO userDTO = context.getBean(SessionService.class).getUser(sessionDTO);
        @NotNull final User user = new User(userDTO);
        @NotNull final Project project = new Project(projectDTO);
        project.setUser(user);
        projectRepository.add(project);
    }

    @Override
    public void create(@Nullable final SessionDTO sessionDTO, @Nullable final String name) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        add(sessionDTO, projectDTO);
    }

    @Override
    public void create(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        projectDTO.setName(name);
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        projectDTO.setDescription(description);
        add(sessionDTO, projectDTO);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findUserProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final List<Project> projects = projectRepository.findUserProjects(userId);
        if (projects == null || projects.isEmpty()) throw new ProjectListEmptyException();
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
            projectsDTO.add(projectDTO);
        }
        return projectsDTO;
    }

    @Override
    @Transactional
    public void removeAllUserProjects(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        projectRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public ProjectDTO findProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new ProjectUnknownException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
        return projectDTO;
    }

    @Nullable
    @Override
    public ProjectDTO findProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.findByName(userId, name);
        if (project == null) throw new ProjectUnknownException(name);
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
        return projectDTO;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new ProjectUnknownException();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
        return projectDTO;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeProjectById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.removeById(userId, id);
        if (project == null) throw new ProjectUnknownException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
        return projectDTO;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectRepository.removeByName(userId, name);
        if (project == null) throw new ProjectUnknownException();
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
        return projectDTO;
    }

    @NotNull
    @Override
    public List<ProjectDTO> getAllProjectsDTO() {
        @Nullable final List<Project> projectList = projectRepository.getAllProjects();
        if (projectList == null || projectList.isEmpty()) throw new ProjectUnknownException();
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            @NotNull final ProjectDTO projectDTO = new ProjectDTO(project);
            projectsDTO.add(projectDTO);
        }
        return projectsDTO;
    }

    @NotNull
    @Override
    @Transactional
    public List<ProjectDTO> loadProjects(@Nullable final List<ProjectDTO> projectsDTO) {
        if (projectsDTO == null || projectsDTO.isEmpty()) throw new ProjectUnknownException();
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (@NotNull final ProjectDTO projectDTO : projectsDTO) {
            @NotNull final Project project = new Project(projectDTO);
            projectList.add(project);
        }
        projectRepository.mergeCollection(projectList);
        return projectsDTO;
    }

    @Override
    @Transactional
    public void removeAllProjects() {
        projectRepository.removeAllProjects();
    }

}