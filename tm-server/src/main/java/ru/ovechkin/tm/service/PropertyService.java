package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.ovechkin.tm.api.service.IPropertyService;
import ru.ovechkin.tm.exeption.other.PropertiesInvalidException;

@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("${server.host}")
    private String serverHost;

    @Value("${server.port}")
    private String serverPort;

    @Value("${session.salt}")
    private String sessionSalt;

    @Value("${session.cycle}")
    private Integer sessionCycle;

    @Value("${db.driver}")
    private String jdbcDriver;

    @Value("${db.url}")
    private String url;

    @Value("${db.login}")
    private String username;

    @Value("${db.password}")
    private String password;

    @NotNull
    @Override
    public String getJdbcUrl() {
        @NotNull final String dbUrl = url;
        if (dbUrl == null || dbUrl.isEmpty()) throw new PropertiesInvalidException();
        return dbUrl;
    }

    @NotNull
    @Override
    public String getServiceHost() {
        final String propertyHost = serverHost;
        if (propertyHost == null || propertyHost.isEmpty()) throw new PropertiesInvalidException();
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServicePort() {
        final String propertyPort = serverPort;
        if (propertyPort == null || propertyPort.isEmpty()) throw new PropertiesInvalidException();
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        final String propertiesSalt = sessionSalt;
        if (propertiesSalt == null || propertiesSalt.isEmpty()) throw new PropertiesInvalidException();
        return propertiesSalt;
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return sessionCycle;
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return jdbcDriver;
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        final String propertiesLogin = username;
        if (propertiesLogin == null || propertiesLogin.isEmpty()) throw new PropertiesInvalidException();
        return propertiesLogin;
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        final String propertiesPassword = password;
        if (propertiesPassword == null || propertiesPassword.isEmpty()) throw new PropertiesInvalidException();
        return propertiesPassword;
    }

}