package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractRepository<E extends AbstractEntity> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

}