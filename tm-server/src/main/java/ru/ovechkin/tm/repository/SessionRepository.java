package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.entity.Session;

import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;

@Repository
public class SessionRepository extends AbstractRepository implements ISessionRepository {

    @NotNull
    @Override
    public Session add(@NotNull final Session session) {
        entityManager.persist(session);
        return session;
    }

    @Nullable
    public Session findById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        final TypedQuery<Session> query = entityManager.createQuery(
                "FROM Session WHERE user_id = :userId", Session.class)
                .setParameter("userId", userId);
        if (query == null) return null;
        return query.getResultList();
    }

    public void remove(@NotNull final Session session) {
        entityManager.remove(session);
    }

    public void removeByUserId(@Nullable final String userId) {
        entityManager.createQuery(
                "DELETE Session WHERE user_id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    public boolean contains(@NotNull final String id) {
        findById(id);
        return true;
    }

}