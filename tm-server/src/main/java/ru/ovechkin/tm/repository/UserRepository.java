package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.entity.User;

import javax.persistence.*;
import java.util.List;

@Repository
public class UserRepository extends AbstractRepository implements IUserRepository {

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        entityManager.merge(user);
        return user;
    }

    @Override
    public @Nullable User findById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        final TypedQuery<User> query = entityManager.createQuery(
                "SELECT u FROM User u WHERE login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1);
        if (query == null) return null;
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        entityManager.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public List<User> getAllUsers() {
        @Nullable final TypedQuery<User> query = entityManager.createQuery(
                "FROM User", User.class);
        if (query == null || query.getResultList().isEmpty()) return null;
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<User> mergeCollection(@NotNull final List<User> userList) {
        for (@NotNull final User user : userList) {
            entityManager.persist(user);
        }
        return userList;
    }

    @Override
    public void removeAllUsers() {
        entityManager.createQuery("DELETE FROM User")
                .executeUpdate();
    }

}