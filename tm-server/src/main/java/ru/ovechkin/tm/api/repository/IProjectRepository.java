package ru.ovechkin.tm.api.repository;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.entity.Project;

public interface IProjectRepository {

    void add(@NotNull Project project);

    @Nullable
    List<Project> findUserProjects(@NotNull String userId);

    void removeAll(@NotNull String userId);

    @Nullable
    Project findById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

    @Nullable List<Project> getAllProjects();

    @NotNull List<Project> mergeCollection(@NotNull List<Project> projectList);

    void removeAllProjects();

}