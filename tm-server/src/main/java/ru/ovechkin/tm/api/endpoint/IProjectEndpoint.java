package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void add(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "project", partName = "project") ProjectDTO projectDTO);

    @WebMethod
    void createProjectWithName(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void createProjectWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @Nullable
    @WebMethod
    List<ProjectDTO> findUserProjects(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO
    );

    @WebMethod
    void removeAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @NotNull
    @WebMethod
    ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @NotNull
    @WebMethod
    ProjectDTO removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @NotNull
    @WebMethod
    ProjectDTO removeProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

}