package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull String getServiceHost();

    @NotNull Integer getServicePort();

    @NotNull String getSessionSalt();

    @NotNull Integer getSessionCycle();

    @NotNull String getJdbcDriver();

    @NotNull String getJdbcUrl();

    @NotNull String getJdbcUsername();

    @NotNull String getJdbcPassword();
}