package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    @Nullable
    @Transactional
    UserDTO create(String login, String password);

    @Nullable
    @Transactional
    UserDTO create(String login, String password, Role role);

    @NotNull
    UserDTO findById(String id);

    @Nullable
    UserDTO findByLogin(String login);

    @NotNull
    @Transactional
    UserDTO removeUser(UserDTO userDTO);

    @NotNull
    @Transactional
    UserDTO removeById(String adminId, String id);

    @NotNull
    @Transactional
    UserDTO removeByLogin(String userId, String login);

    @Nullable
    @Transactional
    UserDTO lockUserByLogin(String userId, String login);

    @Nullable
    @Transactional
    UserDTO unLockUserByLogin(String userId, String login);

    @NotNull List<UserDTO> getAllUsersDTO();

    @Transactional
    @NotNull List<UserDTO> loadUsers(@Nullable List<UserDTO> usersDTO);

    @Transactional
    void removeAllUsers();

    @Transactional
    @NotNull UserDTO updateProfileLogin(
            @Nullable String userId,
            @Nullable String newLogin
    );

    @Transactional
    void updatePassword(
            @Nullable String userId,
            @Nullable String currentPassword,
            @Nullable String newPassword
    );

}