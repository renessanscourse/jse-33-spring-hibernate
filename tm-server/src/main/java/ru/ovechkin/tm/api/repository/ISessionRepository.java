package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    @Nullable Session findById(@NotNull String id);

    @Nullable List<Session> findByUserId(@NotNull String userId);

    boolean contains(@NotNull String id);

    @NotNull Session add(@NotNull Session session);

    void remove(@NotNull Session session);

    void removeByUserId(@NotNull String userId);

}