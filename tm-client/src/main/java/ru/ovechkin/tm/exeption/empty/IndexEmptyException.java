package ru.ovechkin.tm.exeption.empty;

public class IndexEmptyException extends RuntimeException {

    public IndexEmptyException() {
        super("Error! Index is empty...");
    }

}