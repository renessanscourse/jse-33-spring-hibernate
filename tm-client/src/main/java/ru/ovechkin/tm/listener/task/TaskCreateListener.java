package ru.ovechkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.TaskEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_CREATE;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.print("ENTER THE PROJECT ID TO WHICH YOU WANT TO ADD THE TASK: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        taskEndpoint.createTaskWithNameAndDescription(sessionDTO, name, description, projectId);
        System.out.println("[OK]");
    }

}