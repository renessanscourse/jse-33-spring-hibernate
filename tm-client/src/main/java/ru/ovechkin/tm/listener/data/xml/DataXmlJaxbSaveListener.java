package ru.ovechkin.tm.listener.data.xml;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.event.ConsoleEvent;
import ru.ovechkin.tm.listener.AbstractListener;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class DataXmlJaxbSaveListener extends AbstractListener {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.DATA_XML_JAXB_SAVE;
    }

    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlJaxbSaveListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[DATA XML SAVE]");
        storageEndpoint.dataXmlJaxbSave(sessionDTO);
        System.out.println("[OK]");
    }

}